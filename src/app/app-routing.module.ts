import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ClockComponent } from './clock/clock.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { StockParamsComponent } from './stock-params/stock-params.component';


const routes: Routes = [
  {path:'', component: HomeComponent},
  {path:'about', component:AboutComponent},
  {path:'info', redirectTo: '/about', pathMatch: 'full'},
  {path: 'clock', component:ClockComponent, data:{title: 'Local Time Snapshot', content: 'Its time for a coffee break :)'}},
  {path: 'stock', component:StockParamsComponent},
  {path: 'stock/:id', component:StockParamsComponent},
  {path:'**', component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
