import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-stock-params',
  templateUrl: './stock-params.component.html',
  styleUrls: ['./stock-params.component.css']
})
export class StockParamsComponent implements OnInit {

  stockID

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.stockID = this.route.snapshot.paramMap.get('id')
  }

}
